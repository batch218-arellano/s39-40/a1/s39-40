// dependencies
const express = require("express");
const auth = require ("../auth.js");


const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.get("/details", (req,res) => {
	userController.detailsUser(req.body).then(resultFromController => res.send(resultFromController))
})

/* 
router.post("/enroll", (req,res) => {
	userController.enroll(req.body).then(resultFromController => res.send(resultFromController))
})

*/ 
// S41 Activity 
// Enroll a user



router.post("/enroll", auth.verify,(req,res) => {
	let data = {
		userId: auth.decode(req.headers.authoriztion).id,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;