const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema ({

	firstName:{
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	}, 
	IsAdmin: {
		type: Boolean,
		default: false
	}, 
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	}, 

	// The "enrollments" property/ field will be an array containing  the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course. 
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is not generated"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Enrolled'
			}
		}


		]

})

/*
// Output: 

	{
	"firstName" : "Jaye",
	"lastName" : "Arellano",
	"enrollments" : [
		{
			"courseId" : "crs001",
			"enrolledOn" : "July 1, 2022",
			"status" : "Enrolled"
		},

		{
			"courseId" : "crs002",
			"enrolledOn" : "July 1, 2022",
			"status" : "Enrolled"
		}

		]

	}
*/

module.exports = mongoose.model("User", userSchema);